###########
# BUILDER #
###########

FROM python:3.10.9-bullseye

LABEL maintainer="Valeriy <lamener@yandex.ru>"

# create the appropriate directories
ENV HOME=/usr/src/app
WORKDIR $HOME

